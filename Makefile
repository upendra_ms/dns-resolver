CC=g++
CFLAGS=-c -Wall -O0 -g

all: myresolver

myresolver: myresolver.o
	$(CC) myresolver.o dns_messages.o -o myresolver

myresolver.o: dns_messages.o myresolver.cc myresolver.h
	$(CC) -c myresolver.cc

dns_messages.o: dns_messages.cc dns_messages.h
	$(CC) -c dns_messages.cc

clean:
	rm -f *.o myresolver
